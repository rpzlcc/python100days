"""Extract datetimes from log entries and calculate the time
   between the first and last shutdown events"""

from datetime import datetime
import os
import urllib.request

SHUTDOWN_EVENT = 'Shutdown initiated'

# prep: read in the logfile
logfile = os.path.join('/tmp', 'log')
urllib.request.urlretrieve('http://bit.ly/2AKSIbf', logfile)

with open(logfile) as f:
    loglines = f.readlines()


# for you to code:

def convert_to_datetime(line):
    """TODO 1:
       Given a log line extract its timestamp and convert it to a datetime object.
       For example calling the function with:
       INFO 2014-07-03T23:27:51 supybot Shutdown complete.
       returns:
       datetime(2014, 7, 3, 23, 27, 51)"""
    datetime_str = line.split()[1]
    line_year = datetime_str.split('T')[0].split('-')[0]
    line_month = datetime_str.split('T')[0].split('-')[1]
    line_day = datetime_str.split('T')[0].split('-')[2]
    line_hour = datetime_str.split('T')[1].split(':')[0]
    line_minute = datetime_str.split('T')[1].split(':')[1]
    line_second = datetime_str.split('T')[1].split(':')[2]
    return datetime(int(line_year), int(line_month), int(line_day), int(line_hour), int(line_minute), int(line_second))


def time_between_shutdowns(loglines):
    """TODO 2:
       Extract shutdown events ("Shutdown initiated") from loglines and calculate the
       timedelta between the first and last one.
       Return this datetime.timedelta object."""
    temp_list = []
    for line in loglines:
        if line.split()[3] == 'Shutdown' and line.split()[4] == 'initiated.':
            temp_list.append(convert_to_datetime(line))
    return temp_list[1] - temp_list[0]


# def convert_to_datetime(line):
#     '''TODO 1:
#        Given a log line extract its timestamp and convert it to a datetime object.
#        For example calling the function with:
#        INFO 2014-07-03T23:27:51 supybot Shutdown complete.
#        returns:
#        datetime(2014, 7, 3, 23, 27, 51)'''
#     timestamp = line.split()[1]
#     date_str = '%Y-%m-%dT%H:%M:%S'
#     return datetime.strptime(timestamp, date_str)
#
#
# def time_between_shutdowns(loglines):
#     '''TODO 2:
#        Extract shutdown events ("Shutdown initiated") from loglines and
#        calculate the timedelta between the first and last one.
#        Return this datetime.timedelta object.'''
#     shutdown_entries = [line for line in loglines if SHUTDOWN_EVENT in line]
#     shutdown_times = [convert_to_datetime(event) for event in shutdown_entries]
#     return max(shutdown_times) - min(shutdown_times)

